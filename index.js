var express       = require('express');
var fs            = require('fs')
var app           = express();
const bodyParser  = require('body-parser');
const path        = require('path');
const PORT        = process.env.PORT || 3000;

app.set('view engine','ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static('public'))
app.use(bodyParser.urlencoded({ extended: false }));


app.get('/',(req, res) => {
      const search = req.query.search;
      fs.readFile('data.txt','utf8', function (err, data) {
      var filterData = data.toString().split('\n').filter(val=>val.indexOf(search) >= 0 );
      if (filterData.length > 0) {
        res.render('index', {bio : filterData});
      }else{
            if (data.length == 0) {
              bio = data
            }else{
              bio = fs.readFileSync('data.txt', 'utf8').split('\n');
            }
            res.render('index', {bio});
        }
      });
});
app.post('/insert',(req,res) => {
        var kd = req.body.kd;
        var name = req.body.nama;
        var jenis_barang = req.body.jb;
        var merk = req.body.merk;
        fs.readFile('data.txt', function(err, data) {
          if (data.length == 0) {
            fs.writeFileSync('data.txt', kd+'#'+name+'#'+jenis_barang+'#'+merk)
          }else{
            fs.appendFileSync('data.txt','\r\n'+kd+'#'+name+'#'+jenis_barang+'#'+merk)
          }
          if(err) throw err;
          res.redirect('/');
        });
});
app.get('/deleteBio/:id',(req,res) => {
  var uid = req.params.id.toString();
  fs.readFile('data.txt','utf8', function(err, data) {
    var newData = data.toString().split('\n').filter(val=>val.indexOf(uid) === -1 ).join('\n');
    fs.writeFile('data.txt',newData,'utf8',function(err) {
        if (err) throw err;
        res.redirect('/');
      });
  });
});
app.post('/update',(req,res) => {
      var id = req.body.id.toString();;
      var kd = req.body.kd;;
      var name = req.body.nama;
      var jenis_barang = req.body.jb;
      var merk = req.body.merk;
      fs.readFile('data.txt','utf8', function(err, data) {
        if (data.split('\n').length > 1) {
          var editData = data.toString().split('\n').filter(val=>val.indexOf(id) > -1 ).join('\n');
          var newData = data.toString().split('\n').filter(val=>val.indexOf(id) === -1 ).join('\n');
          var newValue = editData.replace(editData,kd+'#'+name+'#'+jenis_barang+'#'+merk)
          fs.writeFileSync('data.txt',newValue+'\n'+newData)
          res.redirect('/');
        }else{
          fs.writeFileSync('data.txt', kd+'#'+name+'#'+jenis_barang+'#'+merk)
          res.redirect('/');
        }
      });
});

app.listen(PORT, console.log("Server start in port : " + PORT))
